<?php

use Drupal\Core\Form\FormStateInterface;


function karol_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  // Add a checkbox to toggle the breadcrumb trail.
  
  $form['karol_settings'] = array(
    '#type' => 'details',
    '#title' => t('Karol settings'),
    '#open' => TRUE, // Controls the HTML5 'open' attribute.
  );

  $form['karol_settings']['breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Wyświetlaj tytuł strony'),
    '#default_value' => theme_get_setting('breadcrumb', 'karol'),
    '#description' => t('Pokaż tytuł strony w breadcrumbie.'),
  );

  $form['karol_settings']['header_bg'] = array(
    '#type' => 'file',
    '#title' => t('Upload header background image'),
    '#size' => 40,
    '#attributes' => array('enctype' => 'multipart/form-data'),
    '#description' => t('If you don\'t jave direct access to the server, use this field to upload your header background image. Uploads limited to .png .gif .jpg .jpeg .apng .svg extensions'),
    '#element_validate' => array('karol_header_bg_validate'),
  );




}


/**
 * Check and save the uploaded header background image
 */
function karol_header_bg_validate($element, FormStateInterface $form_state) {
  global $base_url;

  $validators = array('file_validate_extensions' => array('png gif jpg jpeg apng svg'));
  $file = file_save_upload('header_bg', $validators, "public://", NULL, FILE_EXISTS_REPLACE);

  if (!empty($file)) {
    // change file's status from temporary to permanent and update file database
    if ((is_object($file[0]) == 1)) {
      $file[0]->status = FILE_STATUS_PERMANENT;
      $file[0]->save();
      $uri = $file[0]->getFileUri();
      $file_url = file_create_url($uri);
      $file_url = str_ireplace($base_url, '', $file_url);
      $form_state->setValue('header_bg_file', $file_url);
    }
 }
}