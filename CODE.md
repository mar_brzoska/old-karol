
$variables['attributes']['href'] = Url::fromRoute('entity.user.canonical', array(
        'user' => $variables['uid'],
      ))->toString();




  if (!$account) {
    drupal_set_message(t('The user account %id does not exist.', array('%id' => $uid)), 'error');
    \Drupal::logger('user')->error('Attempted to cancel non-existing user account: %id.', array('%id' => $uid));
    return;
  }



----------------------------------------------------------------------------------
$logger = \Drupal::logger('user');
  drupal_set_message(t('%name has been deleted.', array('%name' => $account->getUsername())));
      $logger->notice('Deleted user: %name %email.', array('%name' => $account->getUsername(), '%email' => '<' . $account->getEmail() . '>'));

public static Drupal::logger($channel)
Returns a channel logger object.

Parameters: string $channel: The name of the channel. Can be any string, but the general practice is to use the name of the subsystem calling this.

Return value: \Psr\Log\LoggerInterface The logger for this channel.




<?php

use Drupal\Component\Utility\String;
use Drupal\Component\Utility\SafeMarkup;

/**
 * Implements hook_preprocess_HOOK().
 *
 * Do breadcrumba dodajemy tytuł wyświetlanej strony.
 */
function xkarol_preprocess_breadcrumb(&$variables) {

  // pobieramy tytuł strony
  // https://www.drupal.org/node/2067859
  $request = \Drupal::request();
  $route_match = \Drupal::routeMatch();

  // $title dla stron typ node, user Wyjaśnić to.
  //$title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());
  if ($route = $request->attributes->get(\Symfony\Cmf\Component\Routing\RouteObjectInterface::ROUTE_OBJECT)) {
    $title = \Drupal::service('title_resolver')->getTitle($request, $route);
    //$text = String::checkPlain($title);
    $text = $title;
    // dla strony głównej $title = null?
  }

  // $title dla stron wyświetlających widok terminu taksonomii (np.: /taxonomy/term/1)
  // Sprawdzamy czy jest obiekt Term

  //if ((\Drupal::service('current_route_match')->getRouteName() == 'entity.taxonomy_term.canonical') && $taxonomy_term = \Drupal::service('current_route_match')->getParameter('taxonomy_term')) {

  if (($route_match->getRouteName() == 'entity.taxonomy_term.canonical') && $taxonomy_term = $route_match->getParameter('taxonomy_term')) {

  //if ($taxonomy_term = \Drupal::routeMatch()->getRawParameter('taxonomy_term')) {
   // $title = \Drupal\taxonomy\Entity\Term::load($taxonomy_term)->getName();
    $title = $taxonomy_term->getName();
    $text = safeMarkup::set("<span>" . t('Taxonomy termks: ') . "</span>" . String::checkPlain($title));
  }

  // pobieramy ścieżkę do strony
  $url = \Drupal::url('<current>');
  if (!empty($title)) {
    $variables['breadcrumb'][] = [
      'text' => $text,
      'url' => $url,
    ];
  }
 
  // Usuwamy drugi człon breadcrubma jeżeli = Page not found 
  if (isset($variables['breadcrumb'][1]) &&  $variables['breadcrumb'][1]['url'] == "/system/404") {
    unset($variables['breadcrumb'][1]);
  }
  // Można tak zrobić: gdy url = /system/404 wówczas $breadcrumb = tylko Home

}
#------------------------------------------------------------- 
?>


PREPROCES_BREADCRUMB
<?php
 // Show or hide the Home link.
  if (isset($config['breadcrumb_home']) && $config['breadcrumb_home'] === 0) {
    if ($variables['breadcrumb'][0]['text'] == t('Home') && $variables['breadcrumb'][0]['url'] == "/") {
      array_shift($variables['breadcrumb']);
    }
  }


//$url = \Drupal\Core\Url::fromRoute('<current>');
 // $request = \Drupal::request();
 // if ($route = $request->attributes->get(\Symfony\Cmf\Component\Routing\RouteObjectInterface::ROUTE_OBJECT)) {
  //  $title = \Drupal::service('title_resolver')->getTitle($request, $route);
  //  if (!empty($title)) {
  //    $variables['breadcrumb'][] = [
  //      'text' => String::checkPlain($title),
  //      'url' => $url->toString(),
   //   ];
  //  }
 // }

  //$current_path = \Drupal::request()->attributes->get('_system_path');
  //$path_args = explode('/', $current_path);
  //if ($path_args[0] == 'taxonomy') {



// Tak będzie błąd, gdy nie będzie obiektu Term.
//  if ($title = \Drupal::routeMatch()->getParameter('taxonomy_term')->getName()) {
//    $text = safeMarkup::set("<span>" . t('Taxonomii termin: ') . "</span>" . String::checkPlain($title));
//  }



  // pobieramy ścieżkę do strony
  // https://www.drupal.org/node/2382211
  //$url = \Drupal\Core\Url::fromRoute('<current>'); // można tak
  $url2 = \Drupal::url('<current>'); // i tak
  //echo '> ' . $url->toString() . ' <'; // dla frontpage /node
  echo '> ' . $url2 . ' <'; // dla frontpage /node
  if (!empty($title)) {
    $variables['breadcrumb'][] = [
      'text' => $text,
      //'url' => $url->toString(),
      'url' => $url2,
    ];
  }

  // $title dla stron wyświetlających widok terminu taksonomii (np.: /taxonomy/term/1)
  // Sprawdzamy czy jest obiekt Term
  if ($taxonomy_term = \Drupal::routeMatch()->getRawParameter('taxonomy_term')) {
    $title = \Drupal\taxonomy\Entity\Term::load($taxonomy_term)->getName();
    //$title = \Drupal\taxonomy\Entity\Term::load($path_args[2])->getName();
    //title = taxonomy_term_load($tid)->getName(); // Deprecated
    $text = safeMarkup::set("<span>" . t('Taxonomy termca: ') . "</span>" . String::checkPlain($title));
  }

    echo "<pre>";
    print_r($variables['breadcrumb']);
    echo "</pre>";

   // Usuwamy drugi człon breadcrubma jeżeli = Page not found 
  if (isset($variables['breadcrumb'][1]) && $variables['breadcrumb'][1]['text'] == t('Page not found') && $variables['breadcrumb'][1]['url'] == "/system/404") {
    unset($variables['breadcrumb'][1]);
  }

  // Page title (at_core)
  //if (isset($config['breadcrumb_title']) && $config['breadcrumb_title'] === 1) {
  #  $request = \Drupal::request();
  #  if ($route = $request->attributes->get(\Symfony\Cmf\Component\Routing\RouteObjectInterface::ROUTE_OBJECT)) {
  #    $title = \Drupal::service('title_resolver')->getTitle($request, $route);
  #    if (!empty($title)) {
  #      $variables['breadcrumb'][]['text'] = String::checkPlain($title);
  #    }
  #  }
  //}


  ?>



<?php
/**
 * hook_html_head_alter()
 */
function XXXkarol_html_head_alter(&$head_elements) {
  global $theme;
  // cleartype
  $head_elements['omega_meta_clear_type'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => "cleartype",
      'content' => "on",
    ),
    '#weight' => -998,
  );
  // update viewport tag
  $head_elements['viewport'] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'content' => 'width=device-width, initial-scale=1, maximum-scale=2, minimum-scale=1, user-scalable=yes',
      'name' => 'viewport',
    ),
    '#weight' => -997,
  );
}
?>

<?php
// Ta funkcja chyba nie działa w motywie. Działa w module.

/**
 * Add a renderable array to the top of the page.
 *
 * @param array $page_top
 *   A renderable array representing the top of the page.
 */
function karol_page_top(array &$page_top) {
  $page_top['inny'] = ['#markup' => 'A to inny makup.'];
}
?>


<?php
//hook_block_view_BASE_BLOCK_ID_alter
function karol_block_view_user_login_block_alter(array &$build, \Drupal\Core\Block\BlockPluginInterface $block) {
  // Contextual links on the system_main block would basically duplicate the
  // tabs/local tasks, so reduce the clutter.
  //unset($build['#contextual_links']);
 if ($block->getBaseId() == 'user_login_block') {
//echo "<pre>";
//echo "xxxxxxxxxxxxxxxxxxxxxxxxxx";
recursive($block);
unset($build);
echo "</pre>";
}

echo "<pre>";
recursive($build);
#unset($variables['content']);
echo "</pre>";

}
?>

<?php


function Xkarol_page_attachments(array &$attachments) {
  // Unconditionally attach an asset to the page.
  $attachments['#attached']['library'][] = 'karol/at.fastclick';


  // Conditionally attach an asset to the page.
  //if (!\Drupal::currentUser()->hasPermission('may pet kittens')) {
  //  $attachments['#attached']['library'][] = 'core/jquery';
  //}

  if (\Drupal::currentUser()->isAuthenticated() === FALSE) {
    $attachments['#attached']['library'][] = 'karol/at.windowsize';
  }


}
?>

<?php
function XXkarol_preprocess_image(&$variables) {

echo "<pre>";
print_r($variables);
echo "</pre>";
}
?>

<?php
/**
 * Processes variables for block.tpl.php.
 *
 * Adding a module block class.
 *
 * @todo remove when http://drupal.org/node/1896098 gets in core.
 */
function devel_preprocess_block(&$variables) {
  if (isset($variables['block']) && $variables['block']->module == 'devel') {
    $variables['attributes']['class'][] = drupal_html_class('block-' . $variables['elements']['#block']->get('plugin'));
  }
}
?>


<?php
//hook_block_view_BASE_BLOCK_ID_alter
function XXX_karo_block_view_user_login_block_alter(array &$build, \Drupal\Core\Block\BlockPluginInterface $block) {
  // Contextual links on the system_main block would basically duplicate the
  // tabs/local tasks, so reduce the clutter.
  //unset($build['#contextual_links']);
  if ($block->getBaseId() == 'user_login_block') {
    //unset($build['content']);
    //recursive($build);
  }

}
?>

<?php

function karo_page_attachments_alter(&$page) {
  //unset($page['footer']);

//HelperFunctions::recursive($page);
//#attached <=> [array]
//#post_render_cache <=> [array]

//HelperFunctions::recursive($page['#attached']);
// Wynik:
//library <=> [array]
//html_head_link <=> [array]
//html_head <=> [array]


}
?>


<?
function Xkaro_block_access(\Drupal\block\Entity\Block $block, $operation, \Drupal\user\Entity\User $account, $langcode) {
  // Example code that would prevent displaying the 'Powered by Drupal' block in
  // a region different than the footer.
  if ($operation == 'view' && $block->getPluginId() == 'system_powered_by_block') {
    return AccessResult::forbiddenIf($block->getRegion() != 'footer')->cacheUntilEntityChanges($block);
  }

  // No opinion.
  return AccessResult::neutral();
}
?>


<?php
/**
 * Respond to entity translation deletion.
 *
 * This hook runs once the entity translation has been deleted from storage.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The original entity object.
 *
 * @ingroup entity_crud
 * @see hook_ENTITY_TYPE_translation_delete()
 */
function hook_entity_translation_delete(\Drupal\Core\Entity\EntityInterface $translation) {
  $languages = \Drupal::languageManager()->getLanguages();
  $variables = array(
    '@language' => $languages[$langcode]->name,
    '@label' => $entity->label(),
  );
  \Drupal::logger('example')->notice('The @language translation of @label has just been deleted.', $variables);
}
?>


<?php
// Ta funkcja chyba nie działa w motywie.
/**
 * Add a renderable array to the top of the page.
 *
 * @param array $page_top
 *   A renderable array representing the top of the page.
 */
function karo_page_top(array &$page_top) {
  $page_top['mymodule'] = ['#markup' => 'This is the top.'];
}


/**
 * Add a renderable array to the bottom of the page.
 *
 * @param array $page_bottom
 *   A renderable array representing the bottom of the page.
 */
function karo_page_bottom(array &$page_bottom) {
  $page_bottom['mymodule'] = ['#markup' => 'This is the bottom.'];
}
?>



<?php
$theme = 'karol';
$regions = system_region_list($theme, REGIONS_VISIBLE);
//$regions = system_region_list($theme, REGIONS_ALL);
print_r($regions);
?>



