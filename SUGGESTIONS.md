Node
=======================================

<?php
function node_theme_suggestions_node(array $variables) {
  $suggestions = array();
  $node = $variables ['elements']['#node'];
  $sanitized_view_mode = strtr($variables ['elements']['#view_mode'], '.', '_');

  $suggestions [] = 'node__' . $sanitized_view_mode;
  $suggestions [] = 'node__' . $node->bundle();
  $suggestions [] = 'node__' . $node->bundle() . '__' . $sanitized_view_mode;
  $suggestions [] = 'node__' . $node->id();
  $suggestions [] = 'node__' . $node->id() . '__' . $sanitized_view_mode;

  return $suggestions;
}
?>

<?php
function views_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  $node = $variables ['elements']['#node'];
  if (!empty($node->view) && $node->view->storage->id()) {
    $suggestions [] = 'node__view__' . $node->view->storage->id();
    if (!empty($node->view->current_display)) {
      $suggestions [] = 'node__view__' . $node->view->storage->id() . '__' . $node->view->current_display;
    }
  }
}
?>

<?
0 <=> node__teaser                   # 'node__' . $sanitized_view_mode;
1 <=> node__page                     # 'node__' . $node->bundle();
2 <=> node__page__teaser             # 'node__' . $node->bundle() . '__' . $sanitized_view_mode;
3 <=> node__2                        # 'node__' . $node->id();
4 <=> node__2__teaser                # 'node__' . $node->id() . '__' . $sanitized_view_mode;
5 <=> node__view__moj_widok          # 'node__view__' . $node->view->storage->id();
6 <=> node__view__moj_widok__embed_1 # 'node__view__' . $node->view->storage->id() . '__' . $node->view->current_display;
?>


Views
=======================================

Dla Views jest podobnie:


0. views-view-unformatted.html.twig                     # najogólniejsze
1. views-view-unformatted--moj_widok.html.twig
2. views-view-unformatted--embed-1.html.twig
3. views-view-unformatted--moj_widok--embed-1.html.twig # najdokładniejsze

Z pliku views.theme.inc
<?php
/**
 * @defgroup views_templates Views template files
 * @{
 * Describes various views templates & overriding options.
 *
 * All views templates can be overridden with a variety of names, using
 * the view, the display ID of the view, the display type of the view,
 * or some combination thereof.
 *
 * For each view, there will be a minimum of two templates used. The first
 * is used for all views: views-view.html.twig.
 *
 * The second template is determined by the style selected for the view. Note
 * that certain aspects of the view can also change which style is used; for
 * example, arguments which provide a summary view might change the style to
 * one of the special summary styles.
 *
 * The default style for all views is views-view-unformatted.html.twig.
 *
 * Many styles will then farm out the actual display of each row to a row
 * style; the default row style is views-view-fields.html.twig.
 *
 * Here is an example of all the templates that will be tried in the following
 * case:
 *
 * View, named foobar. Style: unformatted. Row style: Fields. Display: Page.
 *
 * - views-view--foobar--page.html.twig
 * - views-view--page.html.twig
 * - views-view--foobar.html.twig
 * - views-view.html.twig
 *
 * - views-view-unformatted--foobar--page.html.twig
 * - views-view-unformatted--page.html.twig
 * - views-view-unformatted--foobar.html.twig
 * - views-view-unformatted.html.twig
 *
 * - views-view-fields--foobar--page.html.twig
 * - views-view-fields--page.html.twig
 * - views-view-fields--foobar.html.twig
 * - views-view-fields.html.twig
 *
 * Important! When adding a new template to your theme, be sure to flush the
 * theme registry cache!
 *
 * @see \Drupal\views\ViewExecutable::buildThemeFunctions()
 * @}
 */
?>















0 <=> node__teaser
1 <=> node__article
2 <=> node__article__teaser
3 <=> node__2
4 <=> node__2__teaser
5 <=> node__view__frontpage
6 <=> node__view__frontpage__page_1


0 <=> node__image_and_title
1 <=> node__article
2 <=> node__article__image_and_title
3 <=> node__12
4 <=> node__12__image_and_title
5 <=> node__view__karol_recent_content3header
6 <=> node__view__karol_recent_content3header__embed_1



Recent Content Block
=======================================

Widok:
karol_recent_content_type_3





Widok:
karol_image_and_title_type_1: generuje node w display mode Image and title. 



Display mode:

