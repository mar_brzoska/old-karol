<?php
/**
 * @file
 * Contains \Drupal\karol\Plugin\Block\KarolBlock.
 */
namespace Drupal\karol\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;
use Drupal\karo\Functions\HelperFunctions;
use Drupal\node\Entity\NodeType;

/**
 * Provides my custom block.
 *
 * @Block(
 *   id = "karo_hello",
 *   admin_label = @Translation("Client IP"),
 *   category = @Translation("Blocks")
 * )
 */
class KarolBlock extends BlockBase {
  
/**
   * {@inheritdoc}
   */
  public function build() {
    //return array('#markup' => 'Hello Karollo!!!');
    $content = \Drupal::request()->attributes->get('_system_path'));

    if ($node = \Drupal::request()->attributes->get('node')) {
      echo $node->bundle(); // article
      // Pole body
      #$content = $node->get('body')->value;

      // Tag z html'em
      #$content = drupal_render($node->field_tags[0]->view());

      # ID słownika (tu: tags)
      if ($taxonomy_entity = $node->field_tags->entity) {
        $vid = $taxonomy_entity->getVocabularyId();
        #echo $vid;
        $vocabulary_entity = Vocabulary::load($vid);
        echo $vocabulary_entity->label();
        echo $vocabulary_entity->link(); // generuje pełny link do /admin/structure/taxonomy/manage/tags
        echo $vocabulary_entity->url(); // sam url /admin/structure/taxonomy/manage/tags
        echo $vocabulary_entity->bundle(); //taxonomy_vocabulary
      }



      // Tablica z tagami i trochę innych danych
      #$tags_array = $node->field_tags->entity->toArray();
      #print_r($tags_array);

       // poprzez ->enity dostajemy się do obiektu Term
      //if ($field_tags = $node->field_tags->entity) {
       // $taxonomy = $field_tags->getName(); # tag0 tag1
        //$tags = explode(" ", $taxonomy);
       // echo $tags[1]; # tag1
      //}

      if ($field_tags = $node->get('field_tags') && !empty($field_tags)) {

        #$string2 = $node->field_tags[1]->getString();
        #echo "***" . $string2 . "***";

        //$taxonomy = $field_tags->getName(); # tag0 tag1
        // class TaxonomyTermReferenceItem, testujemy jej metody:
        //$string = $field_tags->offsetGet(0)->getValue(); //Array ( [target_id] => 12 )
        //$string = $field_tags->offsetGet(0)->getName(); // 0
        //$string = $field_tags->offsetGet(0)->view(); // poprzez render($string) otrzymamy link to terminu
        //$string = $field_tags->offsetGet(0)->getString(); // tid 4 oraz 11, ok
       

#                 $node->get('field_tags') === Drupal\Core\Field\EntityReferenceFieldItemList
#   $node->get('field_tags')->offsetGet(1) === Drupal\taxonomy\Plugin\Field\FieldType\TaxonomyTermReferenceItem
# \Drupal\taxonomy\Entity\Term::load($tid) === Drupal\taxonomy\Entity\Term

        $tid = $node->get('field_tags')->offsetGet(0)->getString(); // 12
        $term = Term::load($tid);
        $string = $term->getName();
        //echo Term::load($tid)->getDescription();
        //$string = $term_entity->getName(); 
        echo "<h1>" . $string . "</h1>";
      }


      //echo get_class ($node->field_tags); # Drupal\Core\Field\EntityReferenceFieldItemList
      //echo get_class ($node->field_tags[0]); # Drupal\taxonomy\Plugin\Field\FieldType\TaxonomyTermReferenceItem
      //echo get_class ($node->field_tags->entity); # Drupal\taxonomy\Entity\Term
      //echo get_class ($node->field_tags->getEntity()); # Drupal\node\Entity\Node
      //echo get_class ($node->field_tags->getName()); nic?
      //echo get_class ($node->field_tags->value); # Drupal\karo\Plugin\Block\KaroBlock
      //echo get_class ($node->body->value); # nic
      //echo get_class ($node->get('body')->value); # nic

    }


if ($node2 = \Drupal\node\Entity\Node::load(5)) {
$tytul = $node2->label();
$tytul2 = $node2->getTitle();
$typ_node = $node2->getType();

$system_path = $node2->getSystemPath();
$langcode = $node2->language()->getId();
//$alias_path = \Drupal\Core\Path\AliasManager::getAliasByPath($system_path); // źle, tak nie ma obiektu i są błędy non-object
$alias_path =  \Drupal::service('path.alias_manager')->getAliasByPath($system_path);

$owner = $node2->getOwner(); // zwraca Drupal\user\Entity\User Defines the user entity class.
$user_name = $owner->getUserName();
}

$node_types = NodeType::loadMultiple();
HelperFunctions::recursive($node_types);
$comma_separated = 'l';

    $user = \Drupal::currentUser();
    $client_ip = \Drupal::request()->getClientIp();
    return array(
      //'#markup' => t('Hello @user, your IP is @ip', array('@user' => $user->getUsername(), '@ip' => $client_ip)),
      '#markup' => $comma_separated,
    );

  }

} // end class
