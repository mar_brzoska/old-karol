--------------------------------------------------
Slider
-------------------------------------------------

Przestrzeń nazw dla slidera na stronie głównej to "slider". Taką klasę ma sekcja zawierająca ten slider.

Składniki:
─ styl obrazka Slider (635×424) [slider] proporcje 1.5

─ implementacja hook_theme_suggestions_HOOK_alter() tworzaca alternatywny szablon dla
  image-formatter.html.twig:
  - image-formatter--slider.html.twig

─ tryb wyświetlania node: Slider [node.slider]: obrazek wyświetlany jako Slider, bez tagów.

─ widok Slider, tryb wyświetlania Slider. Widok wykorzystuje szablon:
  - views-view-list--slider.html.twig.
  Szablon ten zawiera wszystkie klasy niezbędne do poprawnego wyświetlania slider'a.

─ szablon node--slider.html.twig

─ styl css flexslider-karol.css

─ library w pliku karol.libraries.yml:
flexslider:
  version: VERSION
  js:
    scripts/flexslider/jquery.flexslider.js: {}
    scripts/flexslider/start.js: {}
  css:
    theme:
      css/flexslider.css: {}
      css/flexslider-karol.css: {}
  dependencies:
    - core/jquery
    - core/drupal



-------------------------------------------------
Node - wyświetlanie
-------------------------------------------------

Wyświetlanie tytułu node poprzez szablon node.html.twig:
--------------------------------------------------------

W preprocess_page() określamy wartość zmiennej $variables['title'] = '';, w sytuacji gdy wyświetlany jest node, czyli:
<?php
if ($node = \Drupal::request()->attributes->get('node')) {
  $variables['title'] = '';
}
?>

W szablonie node.html.twig nie dokonujemy sprawdzenia: {% if not page %}. Przypominam że zmienna page to:
<?php
$variables ['page'] = $variables ['view_mode'] == 'full' && node_is_page($node); 
?>

Wyświetlanie poszczególnych elementów node
------------------------------------------

{{ content.field_image }}
{{ content.body }}
{{ content.links }}
{{ content.comment }}
{{ content|without('field_tags', field_image', 'comment', 'links') }}
