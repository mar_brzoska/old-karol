<?php

/**
 * @file
 * Contains \Drupal\karol\Functions\Karol.
 */

namespace Drupal\karol\Functions;


class Karol {

  /**
   * TODO: dodać drugi agrumnent, dzięki któremu otrzmamy: środa lub np. środę) 
   *
   */
  static public function translate_date($date) {
    $english_date = array('Monday', 'Tuesday', 'Wednesday',  'Thursday', 'Friday', 'Saturday', 'Sunday', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $polish_date = array('poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota', 'niedziela', 'stycznia', 'lutego', 'marca', 'kwietnia', 'maja', 'czerwca', 'lipca', 'sierpnia', 'września', 'października', 'listopada', 'grudnia');
    return str_replace($english_date, $polish_date, $date);
  }

  /**
   * Loads
   * @param array $files
   *   Contains alphabetized list of files that will be required.
   */
  static public function inc_load($files) {
    $tp = drupal_get_path('theme', 'karol');
    $file = '';

    // Check file path and '.inc' extension
    foreach($files as $file) {
      $file_path = $tp . '/inc/' . $file;
      if ( strpos($file,'.inc') > 0 && file_exists($file_path)) {
        require_once($file_path);
      }
    }
  }

  // TODO: jak zapobiegać tworzeniu identycznych aliasów?
  // Niżej rozwiązanie prowizoryczne: jeżeli alias istnieje dodajemy id node.
  static public function alias_validate(array &$path, $node) {
    $is_exists = \Drupal::service('path.alias_storage')->aliasExists($path['alias'], $path['langcode']);
    if ($is_exists) {
      $path['alias'] = $path['alias'] . '-' . $node->id();
      drupal_set_message(t('Alias już istnieje. [node:nid] Utworzono inny alias: !path_alias', array('!path_alias' => $path['alias'])), 'error');
    }
  }

  // Adaptivetheme
  // This is bad, Drupal can populate the region but not print anything, perhaps
  // due to user permissions, such as the Tools and Search blocks on a standard install.
  // In effect this breaks the layout, or at the very least disturbs the layout by giving
  // false positives, which results in the wrong layout classes being generated.
  // This is a critical issue in D8 (and D7 also): https://www.drupal.org/node/953034
  static public function setFalseEmptyRegion(&$variables, $hook) {

    $theme = \Drupal::theme()->getActiveTheme()->getName();
    $regions = system_region_list($theme, REGIONS_VISIBLE);
    //$active_regions = array();
    if ($hook == 'html') {
      // foreach (array_expression as $key => $value)
      foreach ($regions as $region_name => $region_label) {
        if (!empty($variables['page'][$region_name])) {
          // Call drupal_render() to determine the regions visibility.
          if (!$region = drupal_render($variables['page'][$region_name])) {
            //$active_regions[] = $region_name;
            //$variables['page'][$region_name] = true;
            //} else {
            $variables['page'][$region_name] = false;
          }
        }
      }
    }

    if ($hook == 'page') {
      // foreach (array_expression as $key => $value)
      foreach ($regions as $region_name => $region_label) {
        if (!empty($variables['page'][$region_name])) {
          \Drupal::logger('Karol')->notice('Stwierdzono niepusty region: %region.', array('%region' => $region_name));
          // Call drupal_render() to determine the regions visibility.
          //if (!$region = drupal_render($variables['page'][$region_name])) {
          $region = drupal_render($variables['page'][$region_name]);
          if (empty($region)) {
            \Drupal::logger('Karol')->notice('Stwierdzono region o zerowej zawartości: %region.', array('%region' => $region_name));
            $variables['page'][$region_name] = FALSE;
            //unset($variables['page'][$region_name]);
          }
        }
      }
    }
  }
  

  static public function plCharset($string) {
    $string = strtolower($string);
    $polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&amp;', '#', ';', '[',']','domena.pl', '(', ')', '`', '%', '”', '„', '…');
    $miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '', '', '', '', '', '', '', '', '');
    $string = str_replace($polskie, $miedzyn, $string);
  
    // usuń wszytko co jest niedozwolonym znakiem
    $string = preg_replace('/[^0-9a-z\-]+/', '', $string);
  
    // zredukuj liczbę myślników do jednego obok siebie
    $string = preg_replace('/[\-]+/', '-', $string);
  
    // usuwamy możliwe myślniki na początku i końcu
    $string = trim($string, '-');

    $string = stripslashes($string);
  
    // na wszelki wypadek
    $string = urlencode($string);
  
    return $string;
  }





} // end class

