<?php

use Drupal\Component\Utility\SafeMarkup;
use Drupal\karol\Functions\Karol;
use Drupal\pogodna\Functions\Pogodna;

/**
 * Implements hook_preprocess_HOOK().
 */
function karol_preprocess_page(&$variables) {

  // Zmienne $variables['page'][$region_name] przyjmą wartość FALSE
  // jeżeli funckcja drupel_render nic nie wygeneruje.
  Karol::setFalseEmptyRegion($variables, 'page');

  // Zmienna title w szablonie page.html.twig będzie pusta,
  // więc nie będzie wyświetlany tytuł.
  // Tytuł noda wyświetlany jest w szablonie noda.
  //if ($node = \Drupal::request()->attributes->get('node')) {
  //  $variables['title'] = '';
  //}
  
  if (PAGE_VARS) { 
    echo Pogodna::recursive($variables, 'page variables', __FUNCTION__, null, true);
    // if (PAGE_VARS) echo Pogodna::recursive($variables['page']['sidebar_first']['karol_search'], 'page variables', __FUNCTION__);
    // if (PAGE_VARS) echo Pogodna::recursive($variables['page']['primary_menu'], 'page variables', __FUNCTION__);
    // if (PAGE_VARS) echo Pogodna::recursive($variables['page']['primary_menu']['karol_main_menu'], 'page variables', __FUNCTION__);

    # user <=> [OBJECT]: Drupal\Core\Session\AccountProxy
    echo $variables['user']->getUsername();
    # lub
    $current_user = \Drupal::currentUser();
    echo $current_user->getUsername();
  }
}


