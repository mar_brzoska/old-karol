<?php

use Drupal\pogodna\Functions\Pogodna;
use Drupal\karol\Functions\Karol;

/**
 * Implements hook_preprocess_HOOK().
 */
function karol_preprocess_html(&$variables) {

  // Zmienne $variables['page'][$region_name] przyjmą wartość FALSE
  // jeżeli funckcja drupal_render nic nie wygeneruje.
  Karol::setFalseEmptyRegion($variables, 'html');

  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['attributes']['class'][] = 'two-sidebars';
  }
  elseif (!empty($variables['page']['sidebar_first'])) {
    $variables['attributes']['class'][] = 'one-sidebar';
    $variables['attributes']['class'][] = 'sidebar-first';
  }
  elseif (!empty($variables['page']['sidebar_second'])) {
    $variables['attributes']['class'][] = 'one-sidebar';
    $variables['attributes']['class'][] = 'sidebar-second';
  }
  else {
    $variables['attributes']['class'][] = 'no-sidebars';
  }

  if (REQUEST_ATTRIBUTES) {
    $request = \Drupal::request()->attributes;
    Pogodna::recursive($request, 'request', __FUNCTION__);
  }

}