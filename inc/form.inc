<?php

use Drupal\pogodna\Functions\Pogodna;

/**
 * Implements hook_form_FORM_ID_form_alter().
 * Custom search block form
 *  No 'submit button'
 *  Use javascript to show/hide the 'search this site' prompt inside of the text field
 */
function karol_form_search_block_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    unset($form['actions']['submit']);
    $form['keys']['#size'] = theme_get_setting('searchbox_size', 'mayo');
    $prompt = t('Search this super site');
    $form['keys']['#default_value'] = $prompt;
    $form['actions']['submit']['#type'] = 'hidden';
    $form['keys']['#attributes'] = array('onblur' => "if (this.value == '') {this.value = '{$prompt}';}", 'onfocus' => "if (this.value == '{$prompt}') {this.value = '';}" );
  }
}

/**
 * hook_form_FORM_ID_alter()
 */
function karol_form_user_login_form_alter(&$form, &$form_state, $form_id) {
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  $config = \Drupal::config($theme . '.settings')->get('settings');

  // Placeholders.
  $form['name']['#attributes']['placeholder'] = $form['name']['#title'];
  $form['pass']['#attributes']['placeholder'] = $form['pass']['#title'];
  $form['name']['#title_display'] = 'invisible';
  $form['pass']['#title_display'] = 'invisible';

  if (FORM_VARS) echo Pogodna::recursive($form, 'form ID', __FUNCTION__);
}


function karol_form_alter(&$form, &$form_state, $form_id) {

  if (FORM_ID) {
    $XXXform['karol_form_id'] = array(
      '#markup' => '<div class="messages messages--status messages--pogodna">' . $form['#form_id'] . '</div>',
      '#weight' => -1000,
    );

    $markup = '#form_id: ' . $form['#form_id'];

    $form['karol_form_id'] = array(
      '#prefix' => '<div class="messages messages--status messages--pogodna">',
      '#type' => 'item',
      '#title' => t('Dane formularza:') . '<br/>',
      '#markup' => $markup,
      '#description' => t('Informację wygenerowała funkcja ') . __FUNCTION__ . '()',
      '#weight' => -1000,
      '#suffix' => '</div>',
    );

    // To niżej nie działa:
    //$form['karol_form_id']['#attached']['library'][] = array('book', 'admin');
    //$form['karol_form_id']['#attached']['library'][] = 'book/admin';
  }

}