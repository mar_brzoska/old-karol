<?php

/**
 * Implements hook_preprocess_HOOK().
 */
function karol_preprocess_comment(&$variables) {
  $comment = $variables['elements']['#comment'];
  $account = comment_prepare_author($comment);
  if (theme_get_setting('features.comment_user_picture')) {
    $variables['user_picture'] = user_view($account, 'icon32');
  }
  else {
    $variables['user_picture'] = array();
  }
}

