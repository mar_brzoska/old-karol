<?php

use Drupal\Core\Template\Attribute;

function karol_preprocess_image_formatter(&$variables) {
  $variables['attributes'] = new Attribute(array('class' => array()));
  $variables['attributes']['class'][] = 'foto';

  // Tak można dodać klasę dla image-style
  //if ($variables ['image_style'] && $variables['image_style'] == 'slider') {
    //$variables['attributes']['class'][] = 'foto';
  //}

  if ($variables['url']) {
    $variables['attributes']['href'] = $variables['url']->toString();
  }
}