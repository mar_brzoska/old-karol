<?php


// to nie działa w motywie
function karol_page_attachments(array &$page) {
  $page['#attached']['html_head'][] = [
    // The data.
    [
      // The HTML tag to add, in this case a <script> tag.
      '#tag' => 'script',
      // The value of the HTML tag, here we want to end up with <script>alert("Hello world!");</script>.
      '#value' => 'alert("Hello world!");',
    ],
    // A key, to make it possible to recognize this HTML <HEAD> element when altering.
    'hello-world'
  ];
}




/**
 * Add attachments (typically assets) to a page before it is rendered.
 *
 * Use this hook when you want to conditionally add attachments to a page.
 *
 * If you want to alter the attachments added by other modules or if your module
 * depends on the elements of other modules, use hook_page_attachments_alter()
 * instead, which runs after this hook.
 *
 * If you try to add anything but #attached and #post_render_cache to the array
 * an exception is thrown.
 *
 * @param array &$attachments
 *   An array that you can add attachments to.
 *
 * @see hook_page_attachments_alter()
 */
function XXXXXkarol_page_attachments_alter(array &$attachments) {


}


/**
 * Alter CSS files before they are output on the page.
 *
 * @param $css
 *   An array of all CSS items (files and inline CSS) being requested on the page.
 * @param \Drupal\Core\Asset\AttachedAssetsInterface $assets
 *   The assets attached to the current response.
 *
 * @see Drupal\Core\Asset\LibraryResolverInterface::getCssAssets()
 */
function karol_css_alter(&$css, \Drupal\Core\Asset\AttachedAssetsInterface $assets) {
  // Remove defaults.css file.
  //unset($css[drupal_get_path('module', 'system') . '/defaults.css']);

  // Individual CSS files to remove.
  // Sometimes we don't want to remove the entire 
  //library (np.: $attachments['#attached']['library'][] = 'karol/at.fastclick';) as it may
  // load dependancies we need, so we can use this more granular
  // approach to remove individual CSS assets.
  $remove = array(
    //'book.theme.css',
    //'comment.theme.css',
    //'file.formatter.generic.css',
    //'search.theme.css',
    //'system.theme.css',
    //'taxonomy.module.css',
  );
  foreach ($remove as $file) {
    if (array_key_exists($file, $css)) {
      unset($css[$file]);
    }
  }
}
