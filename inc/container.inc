<?php

use Drupal\pogodna\Functions\Pogodna;

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function karol_theme_suggestions_container_alter(array &$suggestions, array $variables) {

  //Pogodna::recursive($variables['element']['#parents']);
  // 1 <=> 0
  // 2 <=> format
  // 3 <=> guidelines

  // Dla wszystkich widoków generowanych przez Views
  // pozbywamy się elementu <div class="views-element-container"></div>
  if (isset($variables['element']['#type']) && $variables['element']['#type'] == 'view') { 
    $suggestions[] = 'container__minimal';
  }
  
  // Możemy też tworzyć sugestię dla pojedynczego widoku.
  //if (isset($variables['element']['#name']) && $variables['element']['#name'] == 'inne_wpisy_tego_autora') { 
  //  $suggestions[] = 'container__view';
  //}

  // $variables['element']['#name'] <=> frontpage // dla widoku strony głównej
  // $variables['element']['#name'] <=> inne_wpisy_tego_autora // dla widoku inne_wpisy_tego_autora


  // Container obejmuje też niektóre elemnty formularzy. Dla przycisków:
  // $variables['element']['#type'] <=> actions
  // Nie ma za to elementu $variables['element']['#name']

  // Dla innych elementów #name = container. Czy dla wszystkich???

  // W ten sposób można pozyć się divów dla przycisków, np.: <div class="form-actions form-wrapper" id="edit-actions"> 
  if (isset($variables['element']['#type']) && $variables['element']['#type'] == 'actions') { 
    //$suggestions[] = 'container__minimal';
  }



  return $suggestions;
}



function karol_preprocess_container(&$variables) {
  if (DEBUG) Pogodna::recursive($variables['element'], 'container variables', __FUNCTION__);
}