<?php

/**
 * Prepares variables for feed icon templates.
 *
 * Default template: feed-icon.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - url: An internal system path or a fully qualified external URL of the
 *     feed.
 *   - title: A descriptive title of the feed.
 */
function karol_preprocess_feed_icon(&$variables) {
  $theme_path = drupal_get_path('theme', 'karol');
  $text = t('Subscribe to !feed-title', array('!feed-title' => $variables['title']));
  $variables['icon'] = array(
    '#theme' => 'image__feed_icon',
    '#uri' => $theme_path . '/images/feed.png',
    '#width' => 32,
    '#height' => 32,
    '#alt' => $text,
  );
  //$variables['attributes']['class'] = array('feed-icon');
  $variables['attributes']['title'] = strip_tags($text);
}