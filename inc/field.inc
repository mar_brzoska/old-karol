<?php

use Drupal\pogodna\Functions\Pogodna;

function karol_theme_suggestions_field_alter(array &$suggestions, array $variables) {

  $element = $variables ['element'];
  //Pogodna::recursive($variables);
  
  //Pogodna::recursive($variables['element']);

  # Wynik Pogodna::recursive($variables):
  # element <=> [array]
  # theme_hook_original <=> field

  // Alternatywny szablon uproszczony.
  $fields = array('field_tags',);
  if (!empty($element['#field_name']) && in_array($element['#field_name'], $fields)) { 
    $suggestions[] = 'field__simplified';
  }
  
  // Alternatywny szblon minimalistyczny.
  $fields = array('user_picture',);
  if (!empty($element['#field_name']) && in_array($element['#field_name'], $fields)) { 
    $suggestions[] = 'field__minimal';
  }

  // Możemy zrobić tak, że do każdego trybu wyświetlania robimy szablon lub robimy
  // jeden szablon do wielu trybów wyświetlania.
  //$suggestions [] = 'field__' . $element ['#field_name'] . '__' . $element ['#view_mode'];
  $view_modes = array('teaser', 'slider');
  if ($element['#field_name'] == 'field_image' && in_array($element['#view_mode'], $view_modes)) { 
    $suggestions [] = 'field__' . $element ['#field_name'] . '__first_image';
  }

  return $suggestions;
}
