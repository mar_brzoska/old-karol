<?php

use Drupal\pogodna\Functions\Pogodna;

/**
 * Implements hook_preprocess_HOOK().
 */
function karol_preprocess_views_view_unformatted(&$variables) {
  // Funkcja wyświetla podstawowe informacje o widoku. Przykład:
  // Dane widoku:
  // Szablon: views_view_unformatted
  // Label: Frontpage
  // View id: frontpage
  // Display id: page_1
  // Informację wygenerowała funkcja:
  // karol_preprocess_views_view_unformatted()
  // Więcej o Views suggestions
  $storage = $variables ['view']->storage;
  $view_id = $storage->id();
  $view_label = $storage->label();
  $display_id = $variables ['view']->current_display;
  $theme_hook = $variables['theme_hook_original']; 
  if (VIEWS_DEBUG) Pogodna::views_preprocess_echo($theme_hook, $view_label, $view_id, $display_id, __FUNCTION__);
}


/**
 * Implements hook_preprocess_HOOK().
 */
function karol_preprocess_views_view_list(&$variables) {
  $storage = $variables ['view']->storage;
  $view_id = $storage->id(); # karol_recent_content_type_3
  $view_label = $storage->label(); # Karol Recent content type 3
  $display_id = $variables ['view']->current_display;
  $theme_hook = $variables['theme_hook_original']; 
  if (VIEWS_DEBUG) Pogodna::views_preprocess_echo($theme_hook, $view_label, $view_id, $display_id, __FUNCTION__);
}