<?php

use Drupal\Core\Template\Attribute;
use Drupal\karol\Functions\Karol;
use Drupal\pogodna\Functions\Pogodna;

/**
 * Implements hook_preprocess_HOOK().
 */
function karol_preprocess_node(&$variables) {

  $node = $variables['elements']['#node'];
  //$variables['attributes']['class'][] = 'node';
  //$variables['attributes']['class'][] = 'node-' . $variables['elements']['#view_mode'];






//Pogodna::recursive($variables);


  // Display mode z kalsą view-item.
  // $display_modes_for_view_item_class = array(
  //   'teaser',
  //   'image_and_title',
  // );
  // if (in_array($variables['elements']['#view_mode'], $display_modes_for_view_item_class)) { 
  //   $variables['attributes']['class'][] = 'view-item';
  // }

// to czy node jest wyświetlane w pełni określa zmienna $variables['page'] z template_preprocess_node(),
// nie ma więc potrzeby aby to jeszcze raz sprawdzać.
//$route_match = \Drupal::routeMatch();
//if (($route_match->getRouteName() == 'entity.node.canonical') && $node_object = $route_match->getParameter('node')) {
  if ($variables['page'] && $node->field_image->entity) {
    //$x = $node->field_image->entity->url();
    $variables['attributes']['class'][] = 'is_image';
  }

  // Wprowadzam zmienną 'node_is_page', odpowiadającą zmiennej 'node'
  // z template_preprocess_page().
  // Chodzi o ujednolicenie momentu wyświetlania tytułu strony
  // przez szablony page i node
  // if ($is_node = \Drupal::routeMatch()->getParameter('node')) { // ??????????????????????????????????
  //   $variables ['node_is_page'] = true;
  // }
  //$node = \Drupal::routeMatch()->getParameter('node') ? $variables ['node_is_page'] = true : $variables ['node_is_page'] = false;

  // Initialize new attributes arrays.
  ##$variables['meta_attributes'] = new Attribute(array('class' => array()));
  // add a class if author picture is printing.
  #if (!empty($variables['author_picture'])) {
   # $variables['meta_attributes']['class'][] = 'node__meta--has-author-picture';
  #} else {
    //$variables['meta_attributes']['class'][] = 'node__meta';
    ##$variables['meta_attributes']['class'] = array('node__meta', 'clearfix');
  #}

  //$variables['title_attributes']['class'][] = 'node__title';
  
  // Usuwamy node links gdy widoczny jest formularz komentarza.
  if (!empty($variables['content']['comment']['0']['comment_form'])) {
  # if ($variables['teaser'] || !empty($variables['content']['comment']['0']['comment_form'])) {
  // unset($variables['content']['links']);
  }

  // Unpublished status message.
  if ($variables['node']->isPublished() === FALSE) {
    $variables['title_prefix']['status'] = array(
      '#markup' => t('Node unpublished'),
      '#prefix' => '<p class="unpublished clearfix" aria-label="Status message" role="contentinfo">',
      '#suffix' => '</p>',
    );
  }


  if ($variables['display_submitted']) {
    if (theme_get_setting('features.node_user_picture')) {
      // To change user picture settings (e.g. image style), edit the 'compact'
      // view mode on the User entity. Note that the 'compact' view mode might
      // not be configured, so remember to always check the theme setting first.
      $variables['author_picture'] = user_view($node->getOwner(), 'icon32');
    }
  }

  // Tłumaczymy datę zachowując właściwę odmianę przez przypadki.
  $variables['date'] = Karol::translate_date(format_date($variables['node']->created->value, 'custom', 'l, j F Y'));

  if (NODE_VARS) Pogodna::recursive($node, 'node variables', __FUNCTION__);

}


function karol_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  if (NODE_SUGGESTIONS) Pogodna::recursive($suggestions, 'node suggestions', __FUNCTION__);
}
