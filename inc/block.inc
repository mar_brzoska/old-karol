<?php

use Drupal\Component\Utility\Html;
use Drupal\pogodna\Functions\Pogodna;
/**
 * Implements hook_preprocess_HOOK().
 *
 * TODO: Czy to jest potrzebne i dobre. Zobacz na id bloku contact w stopce.
 */
function karol_preprocess_block(&$variables) {

  // Tworzymy id z plugin_id. Inaczej niż w template_preprocess_block - tam w id zawarta jest nazwa motywu.
  if (!empty($variables['plugin_id'])) {
    //https://api.drupal.org/api/drupal/core%21includes%21common.inc/function/drupal_html_id/8
   # $variables['attributes']['id'] = Html::getUniqueId($variables['plugin_id']);
  }

  #$variables['title_attributes']['class'][] = 'block__title';

  //if ($variables['attributes']['id'] == 'system-main-block') {
    //echo "<pre>";
    //print_r($variables);
    //echo "</pre>";
  //}

  // Remove login block links.
  if ($variables['base_plugin_id'] === 'user_login_block') {
    unset($variables['content']['user_links']);
  }

//if ($variables['plugin_id'] == 'system_menu_block:main') {
// if ($variables['attributes']['id'] == 'views-blockinne-wpisy-tego-autora-block-1') {
 // Pogodna::recursive($variables['elements']);
//}

//if ($variables ['elements']['#configuration']['provider'] == 'views') {
//Pogodna::recursive($variables['attributes']);
//}


  if (BLOCK_VARS) {
    $hook = $variables['elements']['#plugin_id'];
    echo Pogodna::recursivex($variables['elements'], 'block variables', __FUNCTION__, $hook);
  }


} // end function


/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function karol_theme_suggestions_block_alter(array &$suggestions, array $variables) {

  // $blocks_minimal - tablica bloków do generowania poprzez block__minimal.
  $blocks_minimal = array('system_breadcrumb_block',);
  if (in_array($variables['elements']['#plugin_id'], $blocks_minimal)) { 
    $suggestions[] = 'block__minimal';
  }

  // Dla bloków generowanych przez Views systemową alternatywą jest: block--views.html.twig.
  // Określone to jest w funkcji block_theme_suggestions_block():
  // $suggestions [] = 'block__' . $variables ['elements']['#configuration']['provider'];

  // Jeżeli jednak nie chcemy tworzyć dodatkowego szablonu dla Views, a zamiast tego bloki Views
  // generować szablonem miniamal musimy utworzyć własną alternatywę dla bloku Views:
  if ($variables ['elements']['#configuration']['provider'] == 'views') { 
   // $suggestions[] = 'block__minimal';
  }

  
  // Konkretny blok Views generujemy innym szablonem.
  if ($variables ['elements']['#plugin_id'] == 'views_block:inne_wpisy_tego_autora-block_1') { 
    $suggestions[] = 'block__minimal2';
  }

  // Indywidualny szablon dla system_main_block
  if ($variables['elements']['#plugin_id'] == 'system_main_block') {
    $suggestions[] = 'block__system__main';
  }


#echo '<div class="pog">';
#Pogodna::recursive($suggestions);
#echo "</div>";



  
  return $suggestions;


}