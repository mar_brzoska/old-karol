<?php

use Drupal\pogodna\Functions\Pogodna;

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function karol_theme_suggestions_region_alter(array &$suggestions, array $variables) {
  if (REGION_SUGGESTIONS) {
    $hook = $variables['elements']['#region'];
    Pogodna::recursive($suggestions, 'region suggestions', __FUNCTION__, $hook);
  }
}


/**
 * Implements hook_preprocess_HOOK().
 */
function karol_preprocess_region(&$variables) {
  if (REGION_VARS) {
  	$hook = $variables['elements']['#region'];
  	Pogodna::recursive($variables['elements'], 'region variables', __FUNCTION__, $hook);
  }
}
