Wyświetlana jest etykieta Comments nawet wówczas gdy nie ma komentarza ani formularza dodawania komentarza
----------------------------------------------------------------------------------------------------------
> Status: do zrobienia <

Za wyświetlanie etykiety odpowiada szablon field--comment.html.twig, ten fragment

{% if comments and not label_hidden %}
    {{ title_prefix }}
    <h2{{ title_attributes }}>{{ label }}</h2>
    {{ title_suffix }}
  {% endif %}

Prawdopodobnie zmienna comments nie jest pusta mimo braku komentarzy i formularza.


Suggestions
-----------
> Status: do wyjaśnienia <

Jak wygląda w Drupalu 8 sprawa suggestions? Czy są określone jakieś standardowe warianty szablonów?


Formularz kontaktowy
--------------------
> Status: do zrobienia <

Nie wiem jak pozbyć się div'a otaczającego form-item Subject oraz Message.



hook_page_attachments()
-----------------------
> Status: do wyjaśnienia <

FUnkcja nie działa. Działa bez problemu funkcja hook_page_attachments_alter().


drupal_css_alter()
------------------
> Status: do wyjaśnienia <

Jak usunąć css'a dołączonego do motywu?


Błąd w Chrome
-------------
> Status: do wyjaśnienia <

GET http://d8.beta6.9.loc/core/assets/vendor/jquery/jquery.min.map 404 (Not Found)

Co to za plik .map?
To samo dotyczy css.


Settings
--------
> Status: do wyjaśnienia <

theme settings - jak działa w Drupalu 8?


RSS, ikona feed
---------------
> Status: do wjaśnienia <

Dlaczego rss nie wygląda tak jak na prezydent.pl??


Własne polecenia Drush
----------------------
> Status: do zapoznania się <

W module Colorbox 8 jest to rozpisane.


Watchdog
--------
> Status: do wyjaśnienia <

Czy w Drupalu 8 jest możliwość zapisywania komunikatów do pliku,
tak jak watchdog w Drupalu 7.



not found: sites/default/files/js/jquery.min.map;
-------------------------------------------------
not found: http://d8.beta6.13.loc/core/assets/vendor/jquery/jquery.min.map


Breadcrumb - znaki " i tagi html
-------------------------------------------------
Koniecznie przetestować jak wygląda breadcrumb gdy np. tytuł noda jest taki: "Tytuł w cudzysłowach"
lub np. taki: Tytuł z <b>boldem</b>.