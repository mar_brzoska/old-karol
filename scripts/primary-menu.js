(function($) {

  Drupal.behaviors.primary_menu = {
    attach: function (context, settings) {
      responsivePrimaryMenu();
    }
  };

  //Drupal.behaviors.responsiveBartikCollapsibleMenu = {
  //  attach: function (context, settings) {
  function responsivePrimaryMenu() {
  // We can keep menu collapsed up to width maxWidth.
    var maxWidth = 445;
    var listaMenu = '#system-menu-blockmain .menu';
    var linkiMenu = '#system-menu-blockmain .menu a';
    var miejsceNaToggle = '#system-menu-blockmain';
  
    // Do nothing if menu is empty.
    if ($(linkiMenu).length == 0) {
      return;
    }

    // Append toggle link to the main menu if not already exists.
    if ($('a#menu-toggle').length == 0) {
      $(miejsceNaToggle).append('<a href="#" id="menu-toggle">' + Drupal.t('Menu') + '</a>');
    }
    else {
      return;
    }
      
    // Collapse/expand menu by click on link.
    $('a#menu-toggle').click(function() {
      $(listaMenu).slideToggle('fast');
      return false;
    });

    // Restore visibility settings of menu on increasing of windows width over 445px.
    // Media query works with width up to 460px. But I guess we should take into account some padding.
    $(window).resize(function(){
      var w = $(window).width();
      // Remove all styles if window size more than maxWidth and menu is hidden.
      if(w > maxWidth && $(listaMenu).is(':hidden')) {
        $(listaMenu).removeAttr('style');
      }
    });
  }
  
})(jQuery);