=================================================
Skąd się biorą klasy przy node
=================================================

W node.html.twig mamy:
<article{{ attributes }}>

Orzymujemy w kodzie html:
<article data-history-node-id="4" role="article" class="clearfix">

Skąd się biorą:
---------------

data-history-node-id="4" - z modułu Comment:
/**
 * Implements hook_ENTITY_TYPE_view_alter() for node entities.
 */
function comment_node_view_alter(array &$build, EntityInterface $node, EntityViewDisplayInterface $display) {
  if (\Drupal::moduleHandler()->moduleExists('history')) {
    $build['#attributes']['data-history-node-id'] = $node->id();
  }
}

role="article" - z template_preproces_node()

class="clearfix" - z karol_preprocess_node()




=================================================
Atrybuty w kodzie HTML
=================================================

Szablony Drupala zawierają zdefiniowane już zmienne z atrybutami:
- attributes - dla całego szablonu (np. dla bloku)
- title_attributes - dla tytułu
- content_attributes - dla treści

Jeżeli chcemy dodać własną zmienną z atrybutami musimy ją zdefinować.

Defininiowania zmiennej zawiwerającej atrybuty dokonujemy w pliku preprocess. Wcześniej musimy 
zaimportować przestrzeń nazw z klasą Attribute.

Przykłady:

<?php
use Drupal\Core\Template\Attribute;
function stark_preprocess_node(&$variables) {
  $variables['meta_attributes'] = new Attribute(array('id' => 'foo'));
  $variables['meta_attributes']['class'] = array('node__meta'); 
}
?>

<?php
use Drupal\Core\Template\Attribute;
function stark_preprocess_node(&$variables) {
  $variables['meta_attributes'] = new Attribute(array('class' => array()));
  $variables['meta_attributes']['class'][] = 'node__meta';
  $variables['meta_attributes']['id'] = 'foo';
?>

Teraz z szablonie możemy zmienną tą wykorzystać:

<header{{ meta_attributes }}>

co wygeneruje kod:

<header id="foo" class="node__meta"></header>


Dodawanie atrybutów w szablonie
-------------------------------
Jeżeli musi dodać atrybuty w szablonie, do nazwy zmiennej dodajemy funkcje addClass (dla klas) lub setAttribute. Spowoduje to dodanie atrybutów do atrybutów już instejących w zmiennej.

<header{{ meta_attributes.addClass('class-foo').setAttribute('role', 'banner') }}>

w efekcie otrzymamy:

<header id="foo" class="node__meta class-foo" role="banner">


=================================================
Szablony pól
=================================================

Lista pól
---------
Listę pól w systemie znajdziemy pod adresem: admin/reports/fields


Szablon domyślny
----------------
Szablonem domyślnym jest szablon systemowy field.html.twig.


Systemowe wariany szablonów
---------------------------
Dla wybranych pól możemy utworzyć szablony zgodnie z nazwami wariantów szablnów utworzonymi w funkcji
system_theme_suggestions_field():

<?php
function system_theme_suggestions_field(array $variables) {
  $suggestions = array();
  $element = $variables['element'];

  $suggestions[] = 'field__' . $element['#field_type'];
  $suggestions[] = 'field__' . $element['#field_name'];
  $suggestions[] = 'field__' . $element['#entity_type'] . '__' . $element['#bundle'];
  $suggestions[] = 'field__' . $element['#entity_type'] . '__' . $element['#field_name'];
  $suggestions[] = 'field__' . $element['#entity_type'] . '__' . $element['#field_name'] . '__' . $element['#bundle'];

  return $suggestions;
}
?>

Własne wariany szablonów
------------------------
Nazwy własnych wariantów szablonów tworzymy w funkcji hook_theme_suggestions_HOOK_alter(). Doskonale nadaje się do
przygotowania jednego wariantu szablonu dla kilku pól. Przykład:

<?php
function karol_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  $fields = array('field_image', 'field_foto');
  if (!empty($variables['element']['#field_name']) && in_array($variables['element']['#field_name'], $fields)) { 
    $suggestions[] = 'field__minimal';
  }
  return $suggestions;
}
?>




=================================================
template_preprocess_page()
=================================================

Pierwszy poziom danych w tablicy $variables w funkcji template_preprocess_page():
---------------------------------------------------------------------------------

<?php
echo "<pre>";
recursive($variables);
echo "</pre>";
?>

page => [array]
theme_hook_original => page
directory => themes/karol
attributes => [array]
title_attributes => [array]
content_attributes => [array]
title_prefix => [array]
title_suffix => [array]
db_is_active => 1
is_admin => 
logged_in => 
user => [object]
show_messages => 1
title => Fed Cup. A. Radwańska - Kuzniecowa: strach pomyśleć, co się będzie działo
base_path => /
front_page => /
language => [object]
logo => http://d8.beta6.9.loc/themes/karol/logo.svg
site_name => d8.beta6.9.loc
site_slogan => 
is_front => 
action_links => [array]
tabs => [array]
node => [object]
messages => [array]
breadcrumb => [array]


Pierwszy poziom danych w tablicy $variables['page'] w funkcji template_preprocess_page():
-----------------------------------------------------------------------------------------

<?php
echo "<pre>";
recursive($variables['page']);
echo "</pre>";
?>

#type => page
#cache => [array]
sidebar_first => [array]
content => [array]
primary_menu => [array]
secondary_menu => [array]
footer => [array]
#attached => [array]
#post_render_cache => [array]
#title => Fed Cup. A. Radwańska - Kuzniecowa: strach pomyśleć, co się będzie działo
#show_messages => 1
#theme => page
#pre_render => [array]
#defaults_loaded => 1
#children => 
#render_children => 1
sidebar_second => [array]
header => [array]
highlighted => [array]
help => [array]
page_top => [array]
page_bottom => [array]


=====================================================
Jak usunąc blok poprzez funkcję w motywie lub module?
=====================================================


Tak nie działa:

<?php
function karol_preprocess_page(&$variables) {
  
  unset($variables['page']['primary_menu']['karol_powered']);
  unset($variables['page']['sidebar_first']['user_login_block']);
}

?>

=================================================
Dane włączonych motywów
=================================================

Funkcja list_theme() zwraca obiekty reprezentujące włączone motywy w systemie.

<?php
  echo "<pre>";
  //print_r(list_themes());
  $themes = list_themes();
  echo $themes['bartik']->info['name'];
  echo "</pre>";
?>

[bartik] => Drupal\Core\Extension\Extension Object
        (
            [type:protected] => theme
            [pathname:protected] => core/themes/bartik/bartik.info.yml
            [filename:protected] => bartik.theme
            [splFileInfo:protected] => 
            [root:protected] => /var/www/d8.beta6.9.loc
            [subpath] => themes/bartik
            [origin] => core
            [status] => 1
            [info] => Array
                (
                    [name] => Bartik
                    [type] => theme
                    [base theme] => classy
                    [description] => A flexible, recolorable theme with many regions and a responsive, mobile-first layout.
                    [package] => Core
                    [libraries] => Array
                        (
                            [0] => bartik/global-styling
                        )

                    [ckeditor_stylesheets] => Array
                        (
                            [0] => css/base/elements.css
                            [1] => css/base/typography.css
                            [2] => css/components/captions.css
                            [3] => css/components/content.css
                            [4] => css/components/list.css
                            [5] => css/components/table.css
                        )

                    [regions] => Array
                        (
                            [header] => Header
                            [primary_menu] => Primary menu
                            [secondary_menu] => Secondary menu
                            [help] => Help
                            [page_top] => Page top
                            [page_bottom] => Page bottom
                            [featured] => Featured
                            [content] => Content
                            [sidebar_first] => Sidebar first
                            [sidebar_second] => Sidebar second
                            [triptych_first] => Triptych first
                            [triptych_middle] => Triptych middle
                            [triptych_last] => Triptych last
                            [footer_firstcolumn] => Footer first column
                            [footer_secondcolumn] => Footer second column
                            [footer_thirdcolumn] => Footer third column
                            [footer_fourthcolumn] => Footer fourth column
                            [footer] => Footer
                        )

                    [version] => 8.0.0-beta6
                    [core] => 8.x
                    [project] => drupal
                    [datestamp] => 1422443984
                    [engine] => twig
                    [features] => Array
                        (
                            [0] => logo
                            [1] => favicon
                            [2] => name
                            [3] => slogan
                            [4] => node_user_picture
                            [5] => comment_user_picture
                            [6] => comment_user_verification
                        )

                    [screenshot] => core/themes/bartik/screenshot.png
                    [php] => 5.4.5
                    [mtime] => 1422443984
                    [regions_hidden] => Array
                        (
                            [0] => page_top
                            [1] => page_bottom
                        )

                    [dependencies] => Array
                        (
                            [0] => classy
                        )

                )

            [owner] => core/themes/engines/twig/twig.engine
            [prefix] => twig
            [required_by] => Array
                (
                )

            [requires] => Array
                (
                    [classy] => Array
                        (
                            [name] => classy
                        )

                )

            [sort] => -1
            [base_themes] => Array
                (
                    [classy] => Classy
                )

            [libraries] => Array
                (
                    [0] => bartik/global-styling
                )

            [engine] => twig
            [base_theme] => classy
        )


=================================================
Czy moduł jest włączony
=================================================

<?php
  if (\Drupal::moduleHandler()->moduleExists('rdf') == FALSE) {
    echo "NIE";
  } else {
    echo "TAK";
  }
?>


=================================================
Fizyczne położenie plików klas:
=================================================

use Drupal\Core\Template\Attribute - /core/lib/Drupal/Core/Template/Attribute.php
use Drupal\node\NodeViewBuilder - core/modules/node/src/NodeViewBuilder.php
use Drupal\Component\Utility\String - core/lib/Drupal/Component/Utility/String.php

core/lib/Drupal.php






=================================================
Klasa core/lib/Drupal.php
=================================================

Zawiera szereg funkcji, np. currentUser.

<?php
 /**
   * Gets the current active user.
   *
   * @return \Drupal\Core\Session\AccountProxyInterface
   */
  public static function currentUser() {
    return static::$container->get('current_user');
  }
?>

Zastoswanie:

<?php
$user = \Drupal::currentUser();
?>

Funckcja zwraca nam \Drupal\Core\Session\AccountProxyInterface, który z kolei implementowany jest przez
klasę  \Drupal\Core\Session\AccountProxy. Ta klas z kolei zawiera kilka przydatnych funkcji.


=================================================
function hook_node_insert()
=================================================

Podobna funkcja: hook_ENTITY_TYPE_insert()

Respond to creation of a new entity of a particular type. This hook runs once the entity has been stored. Note that hook implementations may not alter the stored entity data.


Przykład zastosowania. Funkcja tworzy alias przy zapisywaniu noda. Entity jest obiektem, w tym przypadku node. Mając ten obiekt mamy dostęp do metod obiektu, np.:
$entity->get('body')->value
$entity->id()
$entity->label()
$entity->bundle() (np. article, page)
$entity->getEntityTypeId() (np. node)

Więcej: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21Entity.php/class/Entity/8



<?php
function karo_node_insert(Drupal\Core\Entity\EntityInterface $entity) {
 $path = array(
      'source' => 'node/' . $entity->id(),
      'alias' => 'node-' . $entity->id(),
      'langcode' => $entity->language()->getId(),
    );
    \Drupal::service('path.alias_storage')->save($path['source'], $path['alias'], $path['langcode']);
}
?>

Medoda language(), czyli jak uzyskać kod języka
-----------------------------------------------

Metoda zwraca obiekt klasy Language. Obiekt klasy Language dysponuje metodą pozwalającą uzyskać kod języka: getId().

<?php
$entity->language()->getId(),
$entity->language()->getname()
?>

Więcej:
https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21Entity.php/function/Entity%3A%3Alanguage/8
https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Language%21Language.php/class/Language/8



=================================================
Pobieranie bieżacego obiektu node
=================================================

<?php
$node = \Drupal::request()->attributes->get('node');
//$node is now a node object
?>

Więcej: https://api.drupal.org/api/drupal/core!lib!Drupal.php/function/Drupal%3A%3Arequest/8







=================================================
core.services.yml
=================================================

core/core.services.yml

Spis wszystkich usług Drupala 8.

https://api.drupal.org/api/drupal/core%21core.services.yml/8


=================================================
class Drupal - Static Service Container wrapper
=================================================

Static Service Container wrapper - czyli nie jet potrzebny obiekt. Obiekty mogą być tworzone w wyniku statycznego wywołania usługi, np.: \Drupal::currentUser() zwraca obiekt bieżącego usera.


https://api.drupal.org/api/drupal/core%21lib%21Drupal.php/class/Drupal/8

The Drupal 8 core code is mostly object-oriented. However, the code in module hooks is still mostly procedural. This presents challenges because Symfony is completely object-oriented!
In Symfony, if you would need to create some functionality that depends on another service, you would create a new service and define the required dependency in the service container. But if you need a particular service in a Drupal hook, you can't use this mechanism as you are not working in a class in the first place!
For this situation, the static Drupal class is created. It can be used to get services in a procedural context by using Drupal::service('{service id}'), or using specific service accessors like Drupal::request(), Drupal::currentUser(), Drupal::entityManager(). The latter have the advantage of providing better code-completion in your editor. Furthermore, it provides some handy helper functions such as Drupal::url(), which is the equivalent of the Drupal 7 url() method.

An example of the node_access function in node.module using the Drupal class:

<?php
function node_access($op, $node, $account = NULL, $langcode = NULL) {
  $access_controller = \Drupal::entityManager()->getAccessController('node');
  ...
  return $access_controller->access($node, $op, $langcode, $account);
}
?>


\Drupal::currentUser()
======================
<?php
public static function currentUser() {
  return static::$container->get('current_user');
}
?>

Gets the current active user. (obiekt klasy AccountProxy)

Return value: \Drupal\Core\Session\AccountProxyInterface

\Drupal\Core\Session\AccountProxyInterface: defines an interface for a service which has the current account stored. Implemented by class AccountProxy (\Drupal\Core\Session\AccountProxy).

$container - The currently active container object, or NULL if not initialized yet.
current_user - service, class Drupal\Core\Session\AccountProxy.


<?php
function karol_preprocess_html(&$variables) {
  // Get current user.
  $user = \Drupal::currentUser();
  // Add role name classes (to allow css based show for admin/hidden from user)
  $roles = $user->getRoles();
  foreach ($roles as $role){
    $variables['attributes']['class'][] = 'role-' . $role;
  }
  echo $user->getUsername();
}
?>

Więcej:
https://api.drupal.org/api/drupal/core%21lib%21Drupal.php/class/Drupal/8
https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Session%21AccountProxyInterface.php/interface/AccountProxyInterface/8


\Drupal::theme()
================
<?php
public static function theme() {
  return static::$container->get('theme.manager');
}
?>

Otrzymujemy obiekt kalsy ThemeManager. Przykład zastosowania:

$theme = \Drupal::theme()->getActiveTheme()->getName();
$path_to_theme = \Drupal::theme()->getActiveTheme()->getPath();


\Drupal::request()
==================

Retrieves the currently active request object, klasy Request

https://api.drupal.org/api/drupal/core%21vendor%21symfony%21http-foundation%21Symfony%21Component%21HttpFoundation%21Request.php/class/Request/8


\Drupal::l()
===========

Renders a link with a given link text and Url object.

<?php
$variables['link'] = \Drupal::l($variables['url_title'], $variables['url']);
?>


\Drupal::routeMatch()
=====================

Retrieves the currently active route match object, klasy CurrentRouteMatch

https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Routing%21CurrentRouteMatch.php/class/CurrentRouteMatch/8


public function CurrentRouteMatch::getParameter
-----------------------------------------------

Returns the processed value of a named route parameter.
Zwraca przetworzoną wartość nazwanego parametru trasy - czyli obiekt, np.:

<?php
$node = \Drupal::routeMatch()->getParameter('node'));
$taxonomy_term = \Drupal::routeMatch()->getParameter('taxonomy_term'));
$user = \Drupal::routeMatch()->getParameter('user'))
?>

For example, the path node/12345 would have a raw node ID parameter value of 12345, while the processed parameter value would be the corresponding loaded node object.


public function CurrentRouteMatch::getRawParameter
--------------------------------------------------

Returns the raw value of a named route parameter. For example, the path node/12345 would have a raw node ID parameter value of 12345.

W poniższym przkładzie, dla np. /taxonomy/term/12, zmienna $taxonomy_term otrzyma wartość 12.

<?php
if ($taxonomy_term = \Drupal::routeMatch()->getRawParameter('taxonomy_term')) {
  $title = \Drupal\taxonomy\Entity\Term::load($taxonomy_term)->getName();
}
?>



=================================================
class AccountProxy
=================================================

Klasa uługi systemowej service current_user

<?php
\Drupal::currentUser();
?>

https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Session%21AccountProxy.php/class/AccountProxy/8


=================================================
Drupal\Core\Path\AliasManager
=================================================

<?php
$alias_path =  \Drupal::service('path.alias_manager')->getAliasByPath($system_path);
?>

=================================================
Drupal\Core\Path\AliasStorage
=================================================

Przkład: Saves a path alias to the database.
<?php
\Drupal::service('path.alias_storage')->save($path['source'], $path['alias'], $path['langcode']);
?>






Jak utworzyć własną usługę (service)?
-------------------------------------



=================================================
Service Container
=================================================

http://symfony.com/doc/current/book/service_container.html


What is a Service?
------------------
Put simply, a Service is any PHP object that performs some sort of "global" task




=================================================
Przykład bloku
=================================================

What would be the best approach in passing services to block plugins? For example if I need to get the current user or the route information etc. I know the docs on this page say to avoid using \Drupal:: inside a class but lets say for some reason I needed access to that request information would it be best practice to do something like this (not a real module or use case, just for example):

<?php
public function build() {
  $user = \Drupal::currentUser()->getUserName;
  $client_ip = \Drupal::request()->getClientIp();
  return array(
    '#markup' => t('Hello @user, your IP is @ip', array('@user' => $user->getUsername(), '@ip' => $client_ip)),
  );
}
?>


=================================================
class EntityReferenceFieldItemList
=================================================

https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Field!EntityReferenceFieldItemList.php/class/EntityReferenceFieldItemList/8

Wywołanie $node->field_tags zwraca obiekt klasy EntityReferenceFieldItemList








=================================================
http://jonathanpatrick.me/blog/safe-markup
=================================================



=================================================
Obiekt node
=================================================

$node = \Drupal::request()->attributes->get('node')

$node = \Drupal::routeMatch()->getParameter('node')




=================================================
class Url
=================================================

Defines an object that holds information about a URL.


public static function Url::fromRoute
-------------------------------------

Creates a new Url object for a URL that has a Drupal route.

Przykład zastosowania:

<?php
 $url = \Drupal\Core\Url::fromRoute('<current>')->toString();
?>

public function Url::toString
-----------------------------

Generates the string URL representation for this Url object.


=================================================
class Unicode

\Drupal\Component\Utility\Unicode
=================================================

Provides Unicode-related conversions and operations.

<?php
$link_title = Unicode::truncate($link_title, $settings['trim_length'], FALSE, TRUE);
?>


=================================================
Entities
=================================================

Not only content data (nodes, taxonomy terms, users) are now entities, but also non-fieldable data such as views, actions, menus, image styles, etc. are all extending the Entity class.

And also bundles such as node types and vocabularies are now entities. The reasoning behind this is that, even though these object types are not fieldable, some other generalized functionality is handy, such as generalized storage in configuration files or automatic route parameter conversion based on the entity name.

The properties of an entity are defined using attributes. For example, it can be specified here whether or not an entity is fieldable or if it should serve as a bundle for another entity.

An entity may or not be saved in configuration files, depending on the controller. If the controller is or extends ConfigStorageController, these will be saved as configuration files.

Although configuration files have benefits, it means that you cannot use database queries on them. If you want to search them, you will have to load all items and use a PHP 'for loop'. If database storage is preferred, for example in the case of nodes and users, the FieldableDatabaseStorageController can be used. This is the case for nodes, users, taxonomy terms, etc. The end result is, of course, that this content can't be transferred via configuration files. To me, it is yet unclear if it is possible to have a fieldable config storage controller!

To get an understanding of the entity properties I suggest that you have a look at the (annotations of the) ImageStyle class, which is a pure configuration-based entity, the (taxonomy) Term, which is a fieldable and database-stored entity, and Vocabulary, which is a bundle entity.


Two families of entity types
============================

Content
• Stored in database
• Canonical workflow: changes on prod constantly

Configuration
• Stored in YAML files, not database
• Canonical workflow: change on dev, deploy to prod Content



Content
-------
Comment
CustomBlock
Feed
File
(Aggregator)Item
MenuLink
Message*
Node
Term
User

Configuration
-------------
Action
Block
Breakpoint
BreakpointGroup
(Contact)Category
CustomBlockType
DateFormat
Editor
EntityDisplay
EntityFormDisplay
EntityFormMode
EntityViewMode
Field
FieldInstance
FilterFormat
ImageStyle
Language
Menu
Migration
NodeType
PictureMapping
RdfMapping
Role
ShortcutSet
Tour
View
Vocabulary


Two families of entity types
Content
-------
• revisionable
• uses content translation API
• fieldable
• all data is a field and follows the field data model
• integrated with REST
• ID is autogenerated, stable, nonrecyclable

Config
------
• not revisionable (use git)
• uses config translation API
• not fieldable
• data is PHP scalars/arrays and follows a type-defined schema
• not integrated with REST (contrib?)
• ID is a machine name that can be edited and recycled




class View - defines a View configuration entity class.
-------------------------------------------------------


class Node - defines the node entity class.
-------------------------------------------

<?php
if ($node = \Drupal\node\Entity\Node::load($nid)) {
  $title = $node->label();
  $title2 = $node->getTitle();
  $node_type = $node->getType();
  $system_path = $node->getSystemPath();
  //$alias = ?????????
  $owner = $node2->getOwner(); // zwraca Drupal\user\Entity\User Defines the user entity class.
  $owner_name = $owner->getUserName();
}
?>


class Term - defines the taxonomy term entity.
----------------------------------------------

Drupal\taxonomy\Entity\Term

<?php
// poprzez ->enity dostajemy się do obiektu Term
  if ($field_tags = $node->field_tags->entity) {
    $taxonomy = $field_tags->getName(); # tag0 tag1
    $tags = explode(" ", $taxonomy);
    echo $tags[1]; # tag1
  }
?>


class Vocabulary - defines the taxonomy vocabulary entity.
----------------------------------------------------------

\Drupal\taxonomy\Entity\Vocabulary




class NodeType
--------------

Defines the Node type configuration entity.

<?php
$node_types = NodeType::loadMultiple();
?>


=================================================
hook_element_info()
=================================================




=================================================
node_is_page();
=================================================

<?php
/**
 * Checks whether the current page is the full page view of the passed-in node.
 *
 * @param \Drupal\node\NodeInterface $node
 *   A node entity.
 *
 * @return
 *   The ID of the node if this is a full page view, otherwise FALSE.
 */
function node_is_page(NodeInterface $node) {
  $route_match = \Drupal::routeMatch();
  if ($route_match->getRouteName() == 'entity.node.canonical') {
    $page_node = $route_match->getParameter('node');
  }
  return (!empty($page_node) ? $page_node->id() == $node->id() : FALSE);
}
?>


=================================================
Using the API
=================================================


Entity API defines some magic methods, like __get(), to allow for fast and easy access to field values. So using the API is very straightforward and the syntax reminds a lot of the pre Drupal 8 era.

Fetching the actual value of the image’s alternative text will be done as below:

<?php
// The most verbose way.
$string = $entity->get('image')->offsetGet(0)->get('alt')->getValue();

// With magic added by the Entity API.
$string = $entity->image[0]->alt;

// With more magic added by Entity API, to fetch the first item
// in the list by default.
$string = $entity->image->alt;
?>


=================================================
class FileFieldItemList
=================================================

Represents a configurable entity file field.



=================================================
EntityReferenceFieldItemList
=================================================

Pole mulit

Defines a item list class for entity reference fields.


=================================================
class TaxonomyTermReferenceItem
=================================================

Jeden item z pola multi

Plugin implementation of the 'term_reference' field type.

=================================================
Klasy typów pól
=================================================

Klasy typów pól definiowane są w modułach w katalogu Plugin/Field/FieldType/NazwaPola.
Np. pole field_image jest utowrzone z pola typu image.



Przykłady:
Drupal\taxonomy\Plugin\Field\FieldType\TaxonomyTermReferenceItem
Drupal\image\Plugin\Field\FieldType\ImageItem
Drupal\file\Plugin\Field\FieldType\FileItem
Drupal\file\Plugin\Field\FieldType\FileFieldItemList


=================================================
Z modułu Node
=================================================

<?php
/**
 * Implements hook_preprocess_HOOK() for HTML document templates.
 */
function node_preprocess_html(&$variables) {
  // If on an individual node page, add the node type to body classes.
  if (($node = \Drupal::routeMatch()->getParameter('node')) && $node instanceof NodeInterface) {
    $variables['node_type'] = $node->getType();
  }
}
?>

<?php
/**
 * Title callback: Displays the node's title.
 *
 * @param \Drupal\node\NodeInterface $node
 *   The node entity.
 *
 * @return
 *   An unsanitized string that is the title of the node.
 *
 * @see node_menu()
 */
function node_page_title(NodeInterface $node) {
  return $node->label();
}
?>


========================================

Drupal::url('block.admin_display') zwraca url do zarządzania blokami.
Drupal::url() - Generates a URL string dla podanej route name.


\\Drupal::routeMatch()->getRouteName() 


-----------------------------------------------------

    case 'entity.node.edit_form':
      $node = $route_match->getParameter('node');
      $type = NodeType::load($node->getType());
      $help = $type->getHelp();
      return (!empty($help) ? Xss::filterAdmin($help) : '');

    case 'node.add':
      $type = $route_match->getParameter('node_type');
      $help = $type->getHelp();
      return (!empty($help) ? Xss::filterAdmin($help) : '');
  }


  W klasie NodeType, która Defines the Node type configuration entity, jest:

  Plugin annotation

<?php
@ConfigEntityType(
  id = "node_type",
  label = @Translation("Content type")
  itd. ....
?>
  CZYLI: argumentem getParameter jest id.


  ===============================================
  class Crypt
  ===============================================

<?php
use Drupal\Component\Utility\Crypt;
  /**
 * Returns the hash string for a context.
 *
 * @param array $context
 *   Context to be hashed.
 * @return string
 *   Hash string for the context.
 */
function xmlsitemap_sitemap_get_context_hash(array &$context) {
  asort($context);
  return Crypt::hashBase64(serialize($context));
}
?>


=================================================
[marek@ASUS ~]# cat /sys/class/dmi/id/product_name
U41JF
=================================================



=================================================
$variables ['css_name'] = Html::cleanCssIdentifier($id);
$variables['attributes']['class'][] = Html::getClass($class);
=================================================



-------------
./core/modules/system/src/Form/ThemeSettingsForm.php:    return 'system_theme_settings';
-----------